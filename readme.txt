=== Mafia ===

Contributors: codepopular
Tags: custom-background, custom-logo, custom-menu, featured-images, threaded-comments, translation-ready, two-columns, custom-header, blog, entertainment, portfolio
Requires at least: 4.8
Tested up to: 5.3.2
Requires PHP: 5.6
Stable tag: 1.0.1
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Mafia is a Multi-concept Corporate Business WordPress Theme exclusively built for consulting business, startup company, finance business. This theme is fully responsive and strongly focuses on the smartphone and tablet experience. This theme has six widgets which helps to meet your website requirements.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

 == Change Logs ==

  Footer Link Updated



